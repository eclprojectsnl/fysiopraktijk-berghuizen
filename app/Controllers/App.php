<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_tax('specialismen')) {
            $term_name = get_term(get_queried_object_id())->name;
            return $term_name;
        }
        if (is_archive()) {
                get_the_archive_title();
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Pagina niet gevonden.', 'sage');
        }
        return get_the_title();
    }

    // Website instellingen
    public function locations()
    {
        if(get_field('general_locations', 'option')) {
            return get_field('general_locations', 'option');
        }
        return false;
    }

    public function socials()
    {
        if(get_field('general_social_media', 'option')) {
            return get_field('general_social_media', 'option');
        }
        return false;
    }

    public function contact()
    {
        if(get_field('general_contact', 'option')) {
            return get_field('general_contact', 'option');
        }
        return false;
    }

    public function head()
    {
        if(get_field('general_header', 'option')) {
            return get_field('general_header', 'option');
        }
        return false;
    }

    public function popup()
    {
        if(get_field('popup', 'option')) {
            return get_field('popup', 'option');
        }
        return false;
    }
}
