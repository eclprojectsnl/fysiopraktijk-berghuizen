{{--
  Template Name: Vestiging Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

  @include('partials.page-header')
  <section class="section section-location">
    <div class="container">
      <div class="row">
          @foreach ($locations as $location)
            <div class="col-12 section-location__wrapper">
              @if($location['general_locations_name'])
                <h2 class="section-location__title">{{str_replace('Locatie', '',$location['general_locations_name'])}}</h2>
              @endif
              <div class="row">
                <div class="col-xl-6 order-xl-2 section-location__image pr-0">
                  @if($location['general_locations_image'])
                  <img src="{{ $location['general_locations_image']['url']}}" alt="{{ $location['general_locations_image']['alt']}}">
                  @endif
                </div>
                <div class="col-xl-6 order-xl-1 section-location__content">
                  @if($location['general_locations_address'])
                  <p>{{$location['general_locations_address']}}</p>
                  @endif

                  @if($location['general_locations_city'])
                  <p>{{$location['general_locations_city']}}</p>
                  @endif

                  @if($location['general_locations_phone'])
                  <a href="#">{{$location['general_locations_phone']}}</a>
                  @endif

                  @if($location['general_locations_hours'])
                  {!! $location['general_locations_hours'] !!}
                  @endif

                  @if($location['general_locations_accessibility'])
                  {!! $location['general_locations_accessibility'] !!}
                  @endif
                </div>
              </div>
            </div>
          @endforeach
      </div>
    </div>
  </section>

  @endwhile
@endsection
