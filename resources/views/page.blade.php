@extends('layouts.app')

@section('content')
  <header>
    @include('partials.page-header')
  </header>
  <section class="section section-single">
    <div class="container">
      <div class="row">
        @while(have_posts()) @php the_post() @endphp
          <div class="col-xl-8 col-12 offset-xl-2 section-single__content content">
          {!! the_content() !!}
          </div>
        @endwhile
      </div>
    </div>
  </section>
@endsection
