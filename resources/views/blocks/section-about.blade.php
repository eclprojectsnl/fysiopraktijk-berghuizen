{{--
  Title: Sectie over ons
  Description: sectie over ons
  Category: sectie
  Icon: groups
  Keywords: over ons sectie blok
  Mode: edit
  Align: full
  PostTypes: page post specialisten specialismen
  SupportsMode: false
  SupportsMultiple: false
--}}

@php
$title = get_field('section_about_title');
$image = get_field('section_about_image');
$content = get_field('section_about_content');
$link = get_field('section_about_link');
$section_about = $title || $image || $content || $link;

@endphp


@if($section_about)
<section class="section section-about">
  <div class="container">
    <div class="row">
      <div class="col-lg-9 offset-lg-2 section-about__col section-about__col--dark @if($title){{'before'}}@endif px-0">
        <div class="section-about__content">
          <{{ $title['toggle'] ?? 'h2' }} class="title-primary section-about__title-primary title-primary ">{{ $title['section_about_title_one'] }}<br><span class="title-primary title-primary--outline">{{ $title['section_about_title_two'] }}</span></{{ $title['toggle'] ?? 'h2' }}>
        </div>
      </div>
    </div>
  </div>
  @if($image['background_image'] || $image['front_image'])
  <div class="section-about__bottom container">
    <div class="row">
      <div class="col-xl-9 col-lg-12 offset-xl-1 offset-0 section-about__col section-about__col--light px-lg-0">
        <div class="row">
          <div class="col-6 col-md-4 col-lg-6 offset-md-0 offset-6 section__image px-0">
            @if( !empty( $image['background_image'] ) )
              <div class="section__image-1">
                  <img src="<?php echo esc_url($image['background_image']['url']); ?>" alt="<?php echo esc_attr($image['background_image']['alt']); ?>" />
              </div>
            @endif
          </div>
          <div class="col-md-8 col-lg-6 px-md-0">
            <div class="content">
              @if($content)
                {!! $content['content'] !!}
              @endif
              @if($link)
                <a href="{{$link['url']}}" @if($link['target']) target="$link['target']"@endif class="button">{{$link['title'] ?? 'Lees meer' }}</a>
              @endif
              @if( !empty( $image['front_image'] ) )
                <div class="section__image section__image-2">
                    <img src="<?php echo esc_url($image['front_image']['url']); ?>" alt="<?php echo esc_attr($image['front_image']['alt']); ?>" />
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif
</section>
@endif
