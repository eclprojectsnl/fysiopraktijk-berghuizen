{{--
  Title: Sectie specialismen
  Description: sectie specialismen
  Category: sectie
  Icon: groups
  Keywords: specialismen sectie blok
  Mode: edit
  Align: wide
  PostTypes: page post specialisten specialismen
  SupportsAlign: ture
  SupportsMode: false
  SupportsMultiple: false
--}}

@php
$group = get_field('section_specialism_group');

@endphp

@if($group)
<section id="specialismen" class="section section-specialisms @if($group['section_specialism_toggle'] && $group['section_specialism_button'] === 'no-button')section-specialisms--{{$group['section_specialism_toggle']}} @endif">
  @if($group)
  <div class="container">
    <div class="row">
      <div class="col-lg-12 section-specialisms__col">
        <div class="section-specialisms__content">
          <{{ $group['toggle'] ?? 'h2'}} class="title-primary--outline section-specialisms__title">{{ $group['title'] }}</{{ $group['toggle'] ?? 'h2'}}>
        </div>
      </div>
    </div>
  </div>
  @endif
  <div class="section-specialisms__bottom container">
    <div class="row">
      <div class="col-lg-12 section-specialisms__col">

        @php

        $terms = get_terms([
          'taxonomy' => 'specialismen',
          'hide_empty' => false,
        ]);
        @endphp

        @if($terms)
          <ul class="section-specialisms__list @if($group['section_specialism_button'])section-specialisms__list-{{$group['section_specialism_button']}} @endif">
            @foreach ($terms as $term)

            @php
              $prefix = $term->taxonomy.'_'.$term->term_id;
              $str_title = str_replace(' | ', '', $term->name);
            @endphp
              <li class="section-specialisms__list-item">
              <a href="{{get_term_link($term->term_id)}}">{{$str_title}} @if(get_field('sub_title', $prefix))<span>{{ get_field('sub_title', $prefix) }}</span>@endif</a>
              </li>
          @endforeach
          </ul>
        @endif
        @if($group['section_specialism_button'] === 'button')
        <a class="button load-more" href="">Bekijk meer</a>
        @endif
      </div>
    </div>
  </div>
</section>
@endif
