@extends('layouts.app')

@section('content')

  @include('partials.archive-header')

  @if (!have_posts())
    {!! get_search_form(false) !!}
  @endif

@php
  $args = array(
    'post_type'   =>    array( 'specialisten' ),
    'order'       =>    'DESC',
	  'orderby'     =>    'date',
  );
  $query = new WP_Query( $args );

@endphp
@include('partials.card')

@endsection
