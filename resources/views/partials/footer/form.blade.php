@php
$form_i = get_field('form_image', 'options');
@endphp

<section class="section section-form px-0">
  <div class="section-form__svg section-form__svg-1">
    <svg xmlns="http://www.w3.org/2000/svg" width="290.371" height="502.87"><g fill="none"><path d="M290.371 502.87L0 0h290.371z"/><path d="M288.371 495.407V2H3.464l284.907 493.407m2 7.463L0 0h290.371v502.87z" fill="#ea5d10"/></g></svg>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xl-10 offset-xl-2 section-form__content px-0">
        @if( !empty( $form_i ) )
          <div class="section-form__image section-form__image-3">
              <img src="<?php echo esc_url($form_i['sizes']['large']); ?>" alt="<?php echo esc_attr($form_i['alt']); ?>" />
          </div>
        @endif
        <div class="gform container">
          <div class="row justify-content-center">
            <div class="section-form__col col-xl-6">
              {!! do_shortcode(get_field('form_shortcode', 'option')) ?? '' !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
