@foreach ($locations as $key => $location)
<div class="section-general__accordion--wrapper col-12" data-default="{{$key}}">
  <div class="row">

    {{-- title --}}
    <div class="col-lg-4 accordion--title">
      <a class="section-general__accordion--toggle d-block @if(!($key === 0)) collapsed @endif" data-toggle="collapse" href="#target{{$key}}" @if($key === 0) aria-expanded="true" @else aria-expanded="false" @endif aria-controls="target{{$key}}">{{$location['general_locations_name']}}</a>
    </div>

    {{-- content --}}
    <div class="col-lg-8 offset-lg-4 section-general__accordion--content collapse @if($key === 0) show @endif" id="target{{$key}}">
      <div class="row">
        <div class="col-7">
          @if($location['general_locations_address'])
            <p>{{$location['general_locations_address']}}</p>
          @endif
          @if($location['general_locations_city'])
            <p>{{$location['general_locations_city']}}</p>
          @endif
          @if($location['general_locations_phone'])
            <a href="tel:{{$location['general_locations_phone']}}">{{$location['general_locations_phone']}}</a>
          @endif
          @if($location['general_locations_hours'])
            <div class="hours">{!! $location['general_locations_hours'] !!}</div>
          @endif

          @if($location['general_locations_accessibility'])
            <div class="accessibility">{!! $location['general_locations_accessibility'] !!}</div>
          @endif

        </div>

        {{-- Locatie afbeelding --}}
        @if($location['general_locations_image'])
          <div class="col-4">
            <img class="section-general__accordion--image" src="{{ $location['general_locations_image']['sizes']['thumbnail']}}" alt="">
          </div>
        @endif

      </div>
    </div>
  </div>
</div>
@endforeach
