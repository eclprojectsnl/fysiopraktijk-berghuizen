@if(get_field('partner_toggle') == true)
<section class="section section-partner">
  <div class="container">
    <div class="row">
      <h2 class="offset-lg-1 title-primary section-partner__title">wij zijn aangesloten bij </h2>
      <div class="col-lg-10 offset-lg-2 section-partner__content">
        <div class="container">
          <div class="row">
            <div class="col-xl col-lg-12 offset-lg-2 section-partner__col">
              @if( have_rows('partners', 'option') )
              <ul class="row">
                @while( have_rows('partners', 'option') )@php the_row() @endphp
                @php $image = get_sub_field('partner_image', 'option'); @endphp
                @if( !empty( $image ) )
                  <img class="col-lg-3 col-8 offset-lg-0 offset-2" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                @endif
                @endwhile
              </ul>
              @endif
            </div>
          </div>
        </div>
    </div>
  </div>
</section>
@endif
