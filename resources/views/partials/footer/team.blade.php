@php
$title = get_field('specialist_title', 'option');
$link = get_field('specialist_link', 'option');
$image = get_field('specialist_image', 'option');
@endphp

@if(get_field('team_toggle') == true)
<section class="section section-team">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 section-team__col">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-xl-5 offset-xl-1 section-team__content">
              <h2 class="title-primary section-team__title">{{$title}}</h2>

              @if($link)
              <a href="{{$link['url']}}" class="button">{{$link['title']}}</a>
              @endif
            </div>
            @if( !empty( $image ) )
              <div class="col-lg-6 section-team__image section-team__image-3 px-0">
                  <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endif
