<section class="section section-general">
	<div class="container">
    <div class="row">
      <div class="col-xl-9 col-lg-12 section-general__content">
				<h2 class="title-primary title-primary--outline section-general__title">Locaties</h2>
				<div class="row section-general__accordion">

				@include('partials.footer.location')

				</div>
			</div>
			<div class="col-xl-3 col-lg-12 section-socials__content">
				<h2 class="title-primary title-primary--outline section-general__title">Social media</h2>
				<ul class="section-general__social">
					@foreach ($socials as $social)
						<li class="section-general__social--item">
							<a href="{{ $social['general_social_media_url']['url'] }}" target="{{ $social['general_social_media_url']['target'] }}">{{$social['general_social_media_name']}}</a>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</section>
