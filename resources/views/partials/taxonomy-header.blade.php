@php
  $image_back = $header['image_back'];
  $image_front = $header['image_front'];
  $content_head = $header['content'];
  $content = get_the_content();
  $title = App::title();
  $sub_title = get_field('sub_title');
@endphp

<section class="section section-about taxonomy-header">
  <div class="container">
    <div class="row">
      <div class="col-lg-9 offset-lg-2 section-about__col @if($title){{"section-about__col--dark"}}@endif">
        <div class="section-about__content d-none d-lg-block">
          <h1 class="title-primary--outline title-primary section-about__title-primary">{!! $title !!} {!! $sub_title !!}</h1>
        </div>
      </div>
    </div>
  </div>
  <div class="section__svg section__svg-1">
      <svg xmlns="http://www.w3.org/2000/svg" width="720" height="720"><g fill="none" stroke="#ea5d10" stroke-width="2"><g transform="translate(122 122)"><circle cx="238" cy="238" r="238" stroke="none"/><circle cx="238" cy="238" r="237"/></g><g><circle cx="360" cy="360" r="360" stroke="none"/><circle cx="360" cy="360" r="359"/></g></g></svg>
  </div>
  <div class="section-about__content d-block d-lg-none">
    <h1 class="title-primary--outline title-primary section-about__title-primary">{!! $title !!} {!! $sub_title !!}</h1>
  </div>
  <div class="section-about__bottom container">
    <div class="row">
      <div class="col-xl-9 col-lg-12 offset-xl-1 offset-0 section-about__col @if($image_back){{ "section-about__col--light"}} @endif">
        <div class="row">
          @if($image_back)
          <div class="col-6 offset-xl-0 offset-6 section__image px-0">
              <div class="section__image-1">
                  <img src="<?php echo esc_url($image_back['sizes']['large']); ?>" alt="<?php echo esc_attr($image_back['alt']); ?>" />
              </div>
            </div>
          @endif
          <div class="col-xl-6 col-lg-12 px-lg-0 @if(empty($image_back)){{'offset-xl-6 offset-0'}}@endif">
            @if($content_head)
              <div class="content">
                <div class="section__svg section__svg-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="165" height="165"><g fill="none"><path d="M51.778 165v-51.778H0V51.778h51.778V0h61.444v51.778H165v61.444h-51.778V165z"/><path d="M111.222 163v-51.778H163V53.778h-51.778V2H53.778v51.778H2v57.444h51.778V163h57.444m2 2H51.778v-51.778H0V51.778h51.778V0h61.444v51.778H165v61.444h-51.778V165z" fill="#ea5d10"/></g></svg>
                </div>
                  {!! $content_head !!}

                  @if( !empty( $image_front ) )
                    <div class="section__image section__image-2">
                        <img src="<?php echo esc_url($image_front['url']); ?>" alt="<?php echo esc_attr($image_front['alt']); ?>" />
                    </div>
                  @endif
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
