<header class="site">
  <nav class="site-header" role="navigation">
    <div class="site-header__container container">
      <div class="row">
        <div class="site-header__brand col-6 col-lg-2 order-1 order-lg-3 px-0">
          <a href="{{ home_url() }}">{!! wp_get_attachment_image($head['general_logo'], 'large') !!}</a>
          <a href="{{ home_url() }}">{!! wp_get_attachment_image($head['general_favicon'], 'large') !!}</a>
        </div>
        <div class="site-header__contact col col-lg-9 order-2 order-lg-1 px-0" href="tel:0545272707">
          <a class="d-flex d-lg-none" href="tel:{{$contact['general_contact_phone']}}">
            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
              <g id="Group_1608" data-name="Group 1608" transform="translate(-132 -147)">
                <circle id="Ellipse_7" data-name="Ellipse 7" cx="25" cy="25" r="25" transform="translate(132 147)" fill="#ea5d10"/>
                <g id="Group_1604" data-name="Group 1604" transform="translate(134.404 149.853)">
                  <path id="Path_800" data-name="Path 800" d="M13.6,16.364A14.294,14.294,0,0,0,18.1,26.5l.072.072a14.5,14.5,0,0,0,10.013,4.477h.12q.4-.325.722-.65a9.335,9.335,0,0,0,2.094-3.225,1.03,1.03,0,0,0-.193-.7,21.1,21.1,0,0,0-4.453-1.42,1.424,1.424,0,0,0-.6.12l-.939,1.492a1.7,1.7,0,0,1-2.383.144,20.83,20.83,0,0,1-2.527-2.166,21.011,21.011,0,0,1-2.166-2.551,2.024,2.024,0,0,1-.409-1.565A1.289,1.289,0,0,1,18,19.686l1.54-.963a1.93,1.93,0,0,0,.072-.554c-.409-1.589-1.011-3.274-1.468-4.549a1.736,1.736,0,0,0-.554-.12h-.1a9.335,9.335,0,0,0-3.225,2.094c-.241.265-.457.505-.674.77Z" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/>
                </g>
              </g>
            </svg>
            <a class="d-lg-flex d-none phone" href="tel:{{$contact['general_contact_phone']}}">{{$contact['general_contact_phone']}}</a>
            <a class="d-lg-flex d-none email" href="mailto:{{$contact['general_contact_email']}}">{{$contact['general_contact_email']}}</a>
          </a>

        </div>
        <div class="site-header__toggle order-3 px-0 d-xl-none">
          <span class="line"></span>
          <span class="line"></span>
          <span class="line"></span>
        </div>
        <div class="site-header__menu col-12 col-lg-10 order-5 px-xl-0">
          @php wp_nav_menu( array( 'container_class' => '', 'theme_location' => 'primary' ) ); @endphp
          <div class="site-header__menu--footer col-12 px-0">
            <ul class="site-header__menu--social d-flex">
              @foreach ($socials as $social)
              <li>
                <a href="{{ $social['general_social_media_url']['url'] }}" target="{{ $social['general_social_media_url']['target'] }}">{{$social['general_social_media_name']}}</a>
              </li>
              @endforeach
            </ul>
            <ul class="site-header__menu--contact d-flex">
              <li><a class="phone" href="tel:{{$contact['general_contact_phone']}}">Bel ons</a></li>
              <li><a class="email" href="mailto:{{$contact['general_contact_email']}}">Mail ons</a></li>
            </ul>
          </div>
        </div>

        <div class="site-header__search col-12 col-lg-3 order-4 order-lg-2 px-0"> {!! get_search_form(); !!}</div>
      </div>
    </div>
  </nav>
  @if(is_front_page())
  <div class="scroll-indicator">
    <p class="scroll-indicator__tag">scroll</p>
    <svg xmlns="http://www.w3.org/2000/svg" width="15.556" height="81.837" viewBox="0 0 15.556 81.837"><g transform="translate(0.707)"><line y1="80" transform="translate(7)" fill="none" stroke="#ea5d10" stroke-width="2"/><path d="M0,0H10V10" transform="translate(14.143 73.352) rotate(135)" fill="none" stroke="#ea5d10" stroke-width="2"/></g></svg>
  </div>
  @endif
</header>

@if(!isset($_COOKIE["popup"]) && $popup['toggle'] == 1)
<div class="pop-up">
  <div class="pop-up__wrapper">
    <div class="pop-up__close">
      <span></span>
      <span></span>
    </div>
    @if($popup['title'])
    <h2 class="pop-up__title title title--primary text--accent">{{$popup['title']}}</h2>
    @endif
    @if($popup['subtitle'])
    <span class="pop-up__sub-title text--dark">{{$popup['subtitle']}}</span>
    @endif
    @if($popup['content'])
    <div class="pop-up__content">
      {!! $popup['content'] !!}
    </div>
    @endif
    @if($popup['link'])
    <a class="button pop-up__button" href="{{$popup['link']['url']}}">{{$popup['link']['title']}}</a>
    @endif
  </div>
</div>
@endif

