@php
  $title = post_type_archive_title($prefix = '', $display = false);
  $middle = strrpos(substr($title, 0, floor(strlen($title) / 2) + 1), ' ') + 1;
  $pageTitleTop = substr($title, 0, $middle);
  $pageTitleBottom = substr($title, $middle);

  $a_content = get_field('archive_content', 'option');
  $image_back = $a_content['image'];
  $content = $a_content['title'];
@endphp

<section class="section section-about archive-header">
  <div class="container">
    <div class="row">
      <div class="col-lg-9 offset-lg-2 section-about__col @if($title) section-about__col--dark @endif">
        <div class="section-about__content d-none d-lg-block">
          <h1 class="title-primary section-about__title-primary">
            {!! $pageTitleTop !!}
            <br>
            <div class="title-primary title-primary--outline">
              {!! $pageTitleBottom !!}
            </div>
          </h1>
        </div>
      </div>
    </div>
  </div>
  <div class="section-about__content d-block d-lg-none">
    <h1 class="title-primary section-about__title-primary">
      {!! $pageTitleTop !!}
      <br>
      <div class="title-primary title-primary--outline">
        {!! $pageTitleBottom !!}
      </div>
    </h1>
  </div>
  <div class="section-about__bottom container">
    <div class="row">
      <div
        class="col-xl-9 col-lg-12 offset-xl-1 offset-0 section-about__col @if($image_back){{ "section-about__col--light"}} @endif">
        <div class="row">
          @if($image_back)
            <div class="col-6 offset-xl-0 offset-6 section__image px-0">
              <div class="section__image-1">
                <img src="<?php echo esc_url($image_back['sizes']['large']); ?>"
                     alt="<?php echo esc_attr($image_back['alt']); ?>"/>
              </div>
            </div>
          @endif
          <div class="col-xl-6 col-lg-12 px-lg-0 @if(empty($image_back)){{'offset-xl-6 offset-0'}}@endif">
            @if($content)
              <div class="content d-none d-xl-block">
                <h2>{{ $content }}</h2>
              </div>
              <h2 class="d-block d-xl-none mt-5">{{ $content }}</h2>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
