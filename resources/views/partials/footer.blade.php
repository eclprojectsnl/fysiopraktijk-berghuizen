<footer class="content-info">

  @if(!(is_search() || is_tax() || is_archive()))
    @include ('partials.footer.team')
    @include ('partials.footer.partners')
  @endif

  @if(!(is_search() || is_404() || get_page_template_slug(get_the_ID())=='views/template-contact.blade.php'))
  @include ('partials.footer.form')
  @endif
  @include ('partials.footer.general')
  @include ('partials.footer.copyright')
</footer>
