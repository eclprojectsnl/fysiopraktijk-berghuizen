@php
$archive_content = get_field('archive_title', 'option');
@endphp

@if( have_posts() )
<section class="section section-specialism">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="offset-lg-1 title section-specialism__title">
          @if(!(is_post_type_archive('specialisten')))
            Onze <span class="title title--accent">{!! App::title() !!}</span> specialisten
          @else
            {{ $archive_content['title'] }}
          @endif
        </h2>
      </div>
      <div class="col-lg-10 offset-lg-2 section-specialism__col">
        <div class="row">
          @while ( have_posts() )
          @php the_post();

          $terms = get_the_terms( get_the_ID(), 'specialismen' ); // get an array of all the terms as objects.
          $header = get_field('header_additional');
          $image = $header['image_back'];
          $mail = get_field('email');
          $email = strtok(get_the_title(), ' ')
          @endphp

          <div class="col-xl-4 col-md-6 section-specialism__wrapper">
            <div class="section-specialism__card">
              <div class="section-specialism__row row">
                @if($image)
                <div class="section-specialism__image col-4">
                  <img src="{{esc_url($image['sizes']['medium']) }}" alt="">
                </div>
                @endif
                <div class="section-specialism__content col-8 px-0">
                  <a class="section-specialism__content--name" href="{{ the_permalink() }}">{{ the_title() }}</a>
                  <div class="section-specialism__content--terms">
                    @if($terms)
                      @foreach ($terms as $term)
                        <a href="{!! get_term_link($term->term_id) !!}">{!! $term->name !!}</a>
                      @endforeach
                    @endif
                  </div>
                  @if($mail)
                    <div class="section-specialism__content--mail">
                      <a href="mailto:{{$mail}}">Mail {{ strtok(the_title(), " ") }}</a>
                    </div>
                  @else
                    <div class="section-specialism__content--mail">
                      <a href="mailto:{{$email}}@fysiopraktijkberghuizen.nl">Mail {{$email}}</a>
                    </div>
                  @endif
                </div>
              </div>
            </div>
          </div>
          @endwhile
        </div>
      </div>
    </div>
  </div>
</section>
@endif
