<article @php post_class() @endphp>
  <header>
    @include('partials.page-header')
  </header>
  <section class="section section-page">
    <div class="container">
      <div class="row">
        <div class="col-xl-8 col-12 offset-xl-2 section-single__content content">
          {!! the_content() !!}
        </div>
      </div>
    </div>
  </section>
</article>
