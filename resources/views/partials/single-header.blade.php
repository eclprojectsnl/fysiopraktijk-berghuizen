@php
$header = get_field('header_additional');
$image_back = $header['image_back'];
$image_front = $header['image_front'];
$content = $header['content'];
$button = $header['button'];
$timeline = get_field('timeline');


$title = App::title();

if(is_archive()){
  $prefix = 'Archives: ';
  if (substr($title, 0, strlen($prefix)) == $prefix) {
    $title = substr($title, strlen($prefix));
    $titleSplit = strrpos($title, ' ');

    $titleTop    = substr($title, 0, $titleSplit);
    $titleBottom =  substr($title, strrpos($title, ' ') + 1);
  }
} else {
  $titleSplit = strrpos($title, ' ');

  $titleTop    = substr($title, 0, $titleSplit);
  $titleBottom =  substr($title, strrpos($title, ' ') + 1);
}
@endphp


<section class="section section-about">
  <div class="container">
    <div class="row">
      <div class="col-lg-9 offset-lg-2 section-about__col @if($title){{"section-about__col--dark"}}@endif @if($image_back){{"light"}}@endif">
        <div class="section-about__content d-none d-lg-block">
          <h1 class="title-primary section-about__title-primary">{{ $titleTop }}<br><span class="title-primary title-primary--outline">{{ $titleBottom }}</span></h1>
        </div>
      </div>
    </div>
  </div>
  <div class="section-about__content d-block d-lg-none">
    <h1 class="title-primary section-about__title-primary">{{ $titleTop }}<br><span class="title-primary title-primary--outline">{{ $titleBottom }}</span></h1>
  </div>
  <div class="section-about__bottom container">
    <div class="row">
      <div class="col-xl-9 col-lg-12 offset-xl-1 offset-0 section-about__col @if($image_back){{ "section-about__col--light"}} @endif">
        <div class="row">
          @if($image_back)
          <div class="col-6 offset-xl-0 offset-6 section__image px-0">
              <div class="section__image-1">
                <img src="<?php echo esc_url($image_back['sizes']['large']); ?>" alt="<?php echo esc_attr($image_back['alt']); ?>" />
              </div>
            </div>
          @endif
          <div class="col-xl-6 col-lg-12 px-lg-0 @if(empty($image_back)){{'offset-xl-6 offset-0'}}@endif">
            @if($content)
              <div class="content">
                  {!! $content !!}
                  @if($timeline)
                  <ul class="timeline">
                    @foreach ($timeline as $time)
                      <li class="timeline__item"><span>{!! $time['year'] !!}</span>{!! $time['event'] !!}</li>
                    @endforeach
                  </ul>
                  @endif

                  @if($link)
                  <a href="{{$link['url']}}" @if($link['target']) target="$link['target']"@endif class="button">{{$link['title'] ?? 'Lees meer' }}</a>
                  @endif

                  @if( !empty( $image_front ) )
                    <div class="section__image section__image-2">
                        <img src="<?php echo esc_url($image_front['url']); ?>" alt="<?php echo esc_attr($image_front['alt']); ?>" />
                    </div>
                  @endif
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
