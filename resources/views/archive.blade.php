@extends('layouts.app')

@section('content')
  @include('partials.archive-header')

  @if (!have_posts())
    {!! get_search_form(false) !!}
  @endif

  @while (have_posts()) @php the_post() @endphp
    @include('partials.content-'.get_post_type())
  @endwhile

  {!! get_the_posts_navigation() !!}
@endsection
