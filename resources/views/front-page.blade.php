@php
$header = get_field('header_home');
$image = $header['image'];
$content = $header['content'];
$button = $header['button'];
@endphp

@extends('layouts.app')
@section('content')
  @while(have_posts()) @php the_post() @endphp
    <section class="section section-head overflow-hidden">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 section-head__image px-0">
            <div class="section-head__svg section-head__svg-1 d-none d-lg-block">
              <svg xmlns="http://www.w3.org/2000/svg" width="720" height="720"><g fill="none" stroke="#ea5d10" stroke-width="2"><g transform="translate(122 122)"><circle cx="238" cy="238" r="238" stroke="none"/><circle cx="238" cy="238" r="237"/></g><g><circle cx="360" cy="360" r="360" stroke="none"/><circle cx="360" cy="360" r="359"/></g></g></svg>
            </div>
            <img src="{{$image['url']}}" alt="{{$image['alt']}}">
            @if($content)
            <div class="container">
              <div class="row">
                <div class="col-lg-11 section-head__col">
                  <div class="section-head__content">

                    {!! $content !!}

                    @if($button)
                      <a class="button" href="{{ $button['url'] }}">test</a>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            @endif
          </div>
        </div>
      </div>
    </section>

    {!! the_content() !!}

    {{-- @include ('partials.template-parts.specialismen')
    @include ('partials.template-parts.about') --}}
    {{-- @include ('partials.template-parts.team') --}}

  @endwhile
@endsection

