{{--
  Template Name: Intramed
--}}

@extends('layouts.app')

@section('content')
  <header>
    @include('partials.page-header')
  </header>
  <section class="section section-single">
    <div class="container">
      <div class="row">
        @while(have_posts()) @php the_post() @endphp
        <div class="col-xl-8 col-12 offset-xl-2 section-single__content content intramed">

          <!-- Zetten van variabelen die later gebruikt gaan worden -->
          <script language="javascript" type="text/javascript">
            var administratie = '103292_ADM01';
            var portalFrame = 'ImPatientPortal';
            var portalRoot = 'https://imweb.intramedonline.nl/';
          </script>
          <!-- Inladen van JavaScript bestand met benodigde functies. -->
          <script src="https://imweb.intramedonline.nl/Scripts/jquery.min.js"
                  type="text/javascript"></script>
          <script src="https://imweb.intramedonline.nl/Scripts/iframeResizer.min.js"
                  type="text/javascript"></script>
          <script language="javascript" type="text/javascript"
                  src="https://imweb.intramedonline.nl/Scripts/demosite.js">
          </script>
          <!-- Definieren van het iframe waar de portal in getoond zal gaan worden. -->
          {!! get_the_content() !!}
          <!-- Portal laden in het iframe volgens de gegeven variabelen hierboven en in de
           request parameters van de overkoepelende pagina -->
          <script language="javascript" type="text/javascript">
            PageLoad();
          </script>

        </div>
        @endwhile
      </div>
    </div>
  </section>
@endsection
