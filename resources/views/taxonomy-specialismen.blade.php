@extends('layouts.app')

@php
  $term   = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
  $prefix = $term->taxonomy.'_'.$term->term_id;
  $header = get_field('header_specialism', $prefix)
@endphp

@section('content')

  @include('partials.taxonomy-header', ['header' => $header])

  <section class="section section-archive">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-7 col-md-10 section-archive__content content">
          {!! (get_field('content', $prefix)) !!}
        </div>
      </div>
    </div>
  </section>

  @include('partials.card')

@endsection
