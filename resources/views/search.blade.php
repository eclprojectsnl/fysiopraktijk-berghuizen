

@extends('layouts.app')

@section('content')
<section class="section-search">
  <div class="container section-search-container">
    <div class="row">
      @php
      global $wp_query;
      $search_result = $wp_query->query_vars;
      $post_type = get_query_var('post_type');
      @endphp


      <div class="col-lg-12 search-found"><p>{!! 'Er zijn ' . $wp_query->found_posts . ' resultaten voor <strong>"' . $search_result['s'] . '"</strong> gevonden' !!}</p></div>
      @if (!have_posts())
      @else
      @while(have_posts()) @php the_post() @endphp
      <article @php(post_class('col-12'))>
        <header>
          <a class="d-block" href="{{ get_permalink() }}">
            {!! the_title() !!}
          </a>
          <time class="updated" datetime="{{ get_post_time('c', true) }}">
            {{ date('d F Y', strtotime(get_the_date())) }}
          </time>

        </header>
      </article>
      @endwhile
      @endif
    </div>
  </div>
</section>

@endsection
