import Cookies from 'js-cookie'

export default {
  init() {

    // Section specialismen load more button
    $('.button.load-more').click(function (event) {
      $('.section-specialisms__list').toggleClass('active');
      event.preventDefault();
      if($('.section-specialisms__list').hasClass('active')){
        $('.load-more').text('Bekijk minder')
      } else {
        $('.load-more').text('Bekijk meer')
      }
    });

    // Bootstrap location accordion
    var collapse = $('.section-general__accordion');

    collapse.on('show.bs.collapse','.collapse', function() {
      collapse.find('.collapse.show').collapse('hide');
    });

    // hamburger menu toggle active
    $('.site-header__toggle').click(function(event) {
      $(this).toggleClass('active');
      $('.site-header__menu').toggleClass('active');
      $('body').toggleClass('menu-open')
      event.preventDefault();
    });
    // Set active class for menu item with children
    $( '.menu-item-has-children' ).on( 'click', function() {
      $(this).toggleClass('active')
    });

    // set cookie for popup
    $(document).on('click', '.pop-up__close, .pop-up__button', function() {
      $('.pop-up').hide();
      Cookies.set('popup', '0', { expires: 7})
    });

    // Check window resize and prepend scroll indicator
    windowSize();

    $(window).on('resize', function(){
      windowSize();
    });

    $('.scroll-indicator').click(function () {
      $('html, body').animate({
        scrollTop: $('#specialismen').offset().top - 100,
      }, 100);
    });
  },

  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};

function windowSize() {
  var windowWidth = window.innerWidth;

  if(windowWidth < 1440) {
    $('.scroll-indicator').detach().prependTo('.section-head__image');
  } else {
    $('.scroll-indicator').detach().prependTo('header.site');
  }
}
