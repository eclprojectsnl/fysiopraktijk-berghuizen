<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__ . '/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__) . '/config/assets.php',
            'theme' => require dirname(__DIR__) . '/config/theme.php',
            'view' => require dirname(__DIR__) . '/config/view.php',
        ]);
    }, true);

/*
 * Register Custom Navigation Walker
*/
function register_navwalker()
{
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}

add_action('after_setup_theme', 'register_navwalker');

register_nav_menus(array(
    'primary' => __('Primary Menu', 'vazquez'),
    'footer' => __('Footer Menu', 'vazquez'),
));

/*
* Create custom logo
*/
function blueprint_custom_logo_setup()
{
    $defaults = array(
        'header-text' => array('site-title', 'site-description'),
    );
    add_theme_support('custom-logo', $defaults);
}

add_action('after_setup_theme', 'blueprint_custom_logo_setup');

/*
* Create custom post type
*/
function create_posttype()
{

    register_post_type(
        'specialisten',
        // CPT Options
        array(
            'labels' => array(
                'name' => __('Onze specialisten'),
                'singular_name' => __('specialist')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'specialisten', 'width-front' => true),
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-groups',
            'supports' => array('title', 'excerpt', 'trackbacks', 'custom-fields', 'page-attributes', 'post-formats'),
            'taxonomies' => array('category', 'tags'),
        ),
    );
    register_post_type(
        'specialisme',
        // CPT Options
        array(
            'labels' => array(
                'name' => __('Onze specialismen'),
                'singular_name' => __('specialisme')
            ),
            'public' => true,
            'supports' => array('title', 'editor', 'custom-fields', 'excerpt', 'page-attributes'),
        ),
    );
}

register_taxonomy(
    'specialismen', # Taxonomy name
    array('specialisten'), # Post Types
    array( # Arguments
        'labels' => array(
            'name' => __('Specialismen', 'fpb'),
            'singular_name' => __('Specialisme', 'fpb'),
            'search_items' => __('Zoek specialisme', 'fpb'),
            'all_items' => __('Alle specialisme', 'fpb'),
            'parent_item' => __('Hoofdcategorie', 'fpb'),
            'parent_item_colon' => __('Hoofdcategorie:', 'fpb'),
            'view_item' => __('Bekijk specialisme', 'fpb'),
            'edit_item' => __('Bewerk specialisme', 'fpb'),
            'update_item' => __('Update specialisme', 'fpb'),
            'add_new_item' => __('Voeg specialisme toe', 'fpb'),
            'new_item_name' => __('Naam nieuwe specialisme', 'fpb'),
            'menu_name' => __('Specialismen', 'fpb'),
        ),
        'hierarchical' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'specialisme'),
    )
);

/*
* Specialists Order & Show Everyone
*/

add_action('pre_get_posts', 'specialists_order');
function specialists_order($query)
{
    if ($query->is_main_query() && is_tax('specialismen')) {
        $query->set('posts_per_page', '-1');
        $query->set('order', 'ASC');
    }
}

/*
* Allow SVG images
*/
add_action('init', 'create_posttype');

function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'Website instellingen',
        'menu_title' => 'Website instellingen',
        'menu_slug' => 'website-settings',
        'capability' => 'edit_posts',
        'icon_url' => 'dashicons-analytics',
        'position' => 2
    ));
}

/*
 *  Search Form
 */
function wp_search_form($form)
{
    $form =
        '<form role="search" method="get" class="search-form" action="/">
  <label>
    <input type="search" class="search-field" placeholder="Vul hier uw zoekopdracht in…" value="" name="s">
  </label>
  <input type="submit" class="search-submit" value="Search">
</form>';
    return $form;
}
add_filter('get_search_form', 'wp_search_form');

//Ajax not scroll to top
add_filter('gform_confirmation_anchor', '__return_true');


function created_specialisme_taxonomy($term_id, $tt_id) {
    error_log('start specialisme taxonomy');
    $taxonomy = 'specialismen';
    $term = get_term_by('id', $term_id, $taxonomy);
    if(empty($term)) return;


    $tax_prefix = $taxonomy.'_'.$term_id;
    $post_type = 'specialisme';

    $title = $term->name;
    $slug = $term->slug;
    $description = $term->description;

    $header_specialismen = get_field('header_specialism', $tax_prefix);

    $subtitle = get_field('sub_title', $tax_prefix);
    $content = get_field('content', $tax_prefix);
    $linked_post = get_field('linked_post', $tax_prefix);

    if(!empty($linked_post)) {
        do_action('edited_specialismen', $term_id, $tt_id);
        return;
    } // trigger the edit function

    $specialismeArgs = [];
    $specialismeArgs['post_title'] = $title;
    $specialismeArgs['post_name'] = $slug;
    $specialismeArgs['post_content'] = $description;
    $specialismeArgs['post_type'] = $post_type;
    $specialismeArgs['post_status'] = 'publish';


    $specialismePost = wp_insert_post($specialismeArgs);
    if(!is_wp_error($specialismePost)) {
        error_log('created specialisme post');
        // Update new post ACF fields
        update_field('header_specialism', $header_specialismen, $specialismePost); // update post fields
        update_field('sub_title', $subtitle, $specialismePost);
        update_field('content', $content, $specialismePost);

        error_log(var_export(get_field('linked_post',$tax_prefix), true));
        error_log(var_export($specialismePost, true));
        error_log(var_export($tax_prefix, true));

        // Update current linked_post acf field for current taxonomy
        update_field('linked_post', (int)$specialismePost, $tax_prefix); // sets linked post on taxonomy
    } else {
        error_log('failed specialisme post');
    }
    error_log('end specialisme taxonomy');
}
add_action('created_specialismen', 'created_specialisme_taxonomy', 20, 2);

function edited_specialisme_taxonomy($term_id, $tt_id) {
    $taxonomy = 'specialismen';
    $term = get_term_by('id', $term_id, $taxonomy);
    if(empty($term)) return;

    $tax_prefix = $taxonomy.'_'.$term_id;
    $post_type = 'specialisme';

    $title = $term->name;
    $slug = $term->slug;
    $description = $term->description;

    $header_specialismen = get_field('header_specialism', $tax_prefix);

    $subtitle = get_field('sub_title', $tax_prefix);
    $content = get_field('content', $tax_prefix);
    $linked_post = get_field('linked_post', $tax_prefix);


    $specialismeArgs = [];
    $specialismeArgs['post_title'] = $title;
    $specialismeArgs['post_name'] = $slug;
    $specialismeArgs['post_content'] = $description;
    $specialismeArgs['post_type'] = $post_type;
    $specialismeArgs['post_status'] = 'publish';

    // Update linked post object
    if(!empty($linked_post)) {
        $specialismeArgs['ID'] = $linked_post;
    } else {
        return;
    }


    $specialismePost = wp_update_post($specialismeArgs);
    if(!is_wp_error($specialismePost)) {
        // Update new post ACF fields
        update_field('header_specialismen', $header_specialismen, $specialismePost); // update post fields
        update_field('sub_title', $subtitle, $specialismePost);
        update_field('content', $content, $specialismePost);

        error_log(var_export(get_field('linked_post',$tax_prefix), true));
        error_log(var_export($specialismePost, true));
        error_log(var_export($tax_prefix, true));

        // Update current linked_post acf field for current taxonomy
        update_field('linked_post', (int)$specialismePost, $tax_prefix); // sets linked post on taxonomy
    }
}
add_action('edited_specialismen', 'edited_specialisme_taxonomy', 20, 2);


// Remove post type specialismen
function remove_menus() {
    if(!current_user_can('administrator')) :
        remove_menu_page('edit.php?post_type=specialisme');
    endif;
}

add_action('admin_menu', 'remove_menus');
